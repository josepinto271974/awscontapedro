
variable "subnet1" {
  type = string
  default = "wwwt2.micro"
}

variable "subnet2" {
  type = string
  default = "wwwt2.micro"
}


variable "snapshot" {
  type = string
  default = "wwwt2.micro"
}

variable "final_snapshot" {
  type = string
  default = "wwwt2.micro"
}

variable "SG_RDS" {
  type = string
  default = "ec2nome"
}


 variable "vpc_ldap_id_p" {
  type = string
  default = ""
}

variable "aws_db_subnet_group_public1" {
  type = string
  default = "wwrwt2.micro"
}