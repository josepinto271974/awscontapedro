variable "pub_subnet" {
  type = map(object({
    cidr_block        = string
    availability_zone = string
  }))
  default = {
    "PubSub1" = {
      cidr_block        = "172.16.1.0/24"
      availability_zone = "eu-west-3a"
    }
    "PubSub2" = {
      cidr_block        = "172.16.3.0/24"
      availability_zone = "eu-west-3b"
    }
  }
}


variable "AZregiaoDC" {
  type = string
  default = "wwrwt2.micro"
}

variable "regiaoDC" {
  type = string
  default = "wwwt2micro"
}

variable "cdir_vpc" {
  type = string
  default = "10.0.0.0/16"
}

variable "subnet_cdir_subpublica_nat_aza" {
  type = string
  default = "10.0.31.0/24"
}

variable "subnet_cdir_subnetpriv_aza" {
  type = string
  default = "10.0.11.0/24"
}




variable "subnet_cdir_subpublica_nat_azb" {
  type = string
  default = "scib-cross-vpc"
}



variable "nome_vpc" {
  type = string
  default = "scib-cross-vpc"
}
variable "igw_name" {
  type = string
  default = "scib-cross-IGW"
}


variable "map_public_ip_on_launch_priv" {
  type = string
  default = "scib-cross-IGW"
}


variable "map_public_ip_on_launch_pub" {
  type = string
  default = "scib-cross-IGW"
}


variable "subnet_nome_subnetpublic_aza" {
  type = string
  default = "subnetpublicaAVAZA"
}

variable "subnet_nome_subnetpriv_aza" {
  type = string
  default = "subnetpublicaAVAZB"
}
variable "aws_vpc_vpc_cross_id" {
  type = string
  default = ""
}

